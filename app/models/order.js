import DS from 'ember-data';

export default DS.Model.extend({
  productID: DS.attr('number'),
  userID: DS.attr('number'),
  date: DS.attr('number'),
  paymentType: DS.attr('string'),
  total: DS.attr('string')
});
