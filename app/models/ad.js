import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  price: DS.attr('number'),
  local: DS.attr('string'),
  description: DS.attr('string'), 
  user_id: DS.attr('number'),
  latitude: DS.attr('string'),
  longitude: DS.attr('string'),
  favorite_id: DS.attr('number'),   
});