import DS from 'ember-data';

export default DS.Model.extend({
  userID: DS.attr('number'),
  name: DS.attr('string'),
  phone: DS.attr('string'),
  email: DS.attr('string'),
  password: DS.attr('string'),
  password_confirmation: DS.attr('string')
});
