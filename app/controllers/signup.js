import Ember from 'ember';

export default Ember.Controller.extend({
  name: null,
  phone: null,
  email: null,
  password: null,
  password_confirmation: null,
  isSave: true,

  isValid: function(){
   var name = this.get('name'),
      phone = this.get('phone'),
      email = this.get('email'),
      password = this.get('password'),
      password_confirmation = this.get('password_confirmation');   
    if(name && phone && email && password && password_confirmation){      
      this.set('isSave', false)
    }else{
      this.set('isSave', true)
    }
  }.observes('name','phone','email','password','password_confirmation'),

	actions: {
    save: function(){
      var controller = this;

      if (this.get('password') !== this.get('password_confirmation')){        
        return 
      }else{
        var user = this.get('store').createRecord('user',{
          name: this.get('name'),
          email: this.get('email'),
          phone: this.get('phone'),
          password: this.get('password'),
          password_confirmation: this.get('password_confirmation')
        });

        user.save().then(function(data){        
          controller.globals.setCurrentProfile(data);
          controller.transitionToRoute('home')
        });
        
      }    

    }
  }
});