import Ember from 'ember';

export default Ember.Controller.extend({
	actions: {
		toggleCategories: function() {
        	$(".categories_table_topic").slideToggle();
		}
	}	
})