import Ember from 'ember';

export default Ember.Controller.extend({
	title: null,
	price: 2,
	description: null,
	local: null,

	showAd: function(ad){
		console.log("I said hey!!",ad);
		this.set("title",ad.get("title"));
		this.set("price",ad.get("price"));
		this.set("description",ad.get("description"));		
	},
  actions: {
    toggleCategories: function() {
          $(".categories_table_topic").slideToggle();
    }
  }
})