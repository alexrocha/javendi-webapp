import Ember from 'ember';

export default Ember.Controller.extend({
  title: null,
  price: null,
  local: null,
  description: null,
  actions: {
    save: function(){
    var controller = this;
      var ad = this.get('store').createRecord('ad',{
        title: this.get('title'),
        price: this.get('price'),
        local: this.get('local'),
        description: this.get('description') 
      });
      ad.save().then(function(data){
       console.log("ad", data);
      });
    }
  }
})