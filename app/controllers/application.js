import Ember from 'ember';

export default Ember.Controller.extend({
	appVersion: Ember.ENV.APP.version,
	inputEmail: null,
	inputPassword: null,
	loggedIn: false,
	profile: null,	
	clearSession: function(){
		var controller = this;
		try{
			this.get('store').find('user', {email: "", password: "", remember_me: "0"}).then(function(users){
				users.get('firstObject').deleteRecord();
				users.get('firstObject').save();
			});
		}catch(e){};
		this.set('inputEmail',null);
		this.set('inputPassword',null);
	    this.set('loggedIn',false);
		this.globals.setCurrentProfile(null);
	},

	actions: {
		toggleCategories: function() {
        	$(".categories_table_topic").slideToggle();
		},
		singIn: function(){
			var controller = this;
			var email = this.get("inputEmail");
			var password = this.get("inputPassword");
			if(!Ember.isEmpty(email) && !Ember.isEmpty(password)){
				this.get('store').find('user', {email: email, password: password}).then(function(users){
					controller.globals.setCurrentProfile(users.get('firstObject'));
					controller.set('profile', controller.globals.getCurrentProfile().email);
					console.log(controller.get('profile'));
					controller.set('loggedIn',true);
				}, function() {
					alert("this email or password are incorrect");
			    });
			} else {
			    var fieldEmpty = "";
	            (Ember.isEmpty(email) && Ember.isEmpty(password))? fieldEmpty = "email and password is empty" : (Ember.isEmpty(email))? fieldEmpty = "email is empty" : fieldEmpty = "password is empty";
	            alert(fieldEmpty);
	        }
		},
		singOut: function() {
	        this.clearSession();
		}
	}	
})
