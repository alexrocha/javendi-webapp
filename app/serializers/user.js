import DS from 'ember-data';

export default DS.ActiveModelSerializer.extend({
  extract: function (store, type, payload, id, requestType) {
    if(requestType == "findQuery"){
        payload['user']['userID'] = payload['user']['id'];
        payload = {'users': [payload['user']]};
    }else{
      payload = { 'users': payload }
    }
    return this._super(store, type, payload, id, requestType);
  },
});