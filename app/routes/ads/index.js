import Ember from 'ember';

export default Ember.Route.extend({
  setupController:function(controller){  	
    controller.get('store').find('ad').then(function(ads){
	    controller.set('ads', ads);
    });
  }
});