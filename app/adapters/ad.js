import ApplicationAdapter from '../adapters/application';

export default ApplicationAdapter.extend({
  namespace: 'ads',

  pathForType: function(type) {
    console.log("path ad",type);
    return '';
  },
  
  findQuery: function(store, type, query) {
    query = {ads: query}
    console.log("find ad",query); 
    // this.namespace = 'users/sign_in';
    // return this.ajax(this.buildURL(type.typeKey), 'POST', { data: query });
  },

  createRecord: function(store, type, record) {
    console.log("create ad",record);
    var data = {
      ad: {
        title: record.get('title'),
        price: record.get('price'),
        local: record.get('local'),
        description: record.get('description'),
        user: 1
      }
    };

    return this.ajax(this.buildURL('add', null, record), "POST", { data: data });
  },

  // updateRecord: function(store, type, record) {
  //   var data = {
  //     id: record.get('id'),
  //     name: record.get('name'),
  //     phone: record.get('phone'),
  //     email: record.get('email')
  //   };
  //   return this.ajax(this.buildURL('update', null, record) + record.get('id'), "PUT", { data: data });
  // },

  // deleteRecord: function(store, type, record) {
  //   this.namespace = 'users/sign_out';
  //   return this.ajax(this.buildURL('delete', null, record), "DELETE", { data: '' });
  // },

});
