import ApplicationAdapter from '../adapters/application';

export default ApplicationAdapter.extend({
  namespace: 'users',

  pathForType: function(type) {
    return '';
  },
  
  findQuery: function(store, type, query) {
    query = {user: query}    
    this.namespace = 'users/sign_in';
    return this.ajax(this.buildURL(type.typeKey), 'POST', { data: query });
  },

  createRecord: function(store, type, record) {
    var data = {
      user: {
        name: record.get('name'),
        phone: record.get('phone'),
        email: record.get('email'),
        password: record.get('password'),
        password_cconfirmation: record.get('password_cconfirmation')
      }
    };

    return this.ajax(this.buildURL('add', null, record), "POST", { data: data });
  },

  updateRecord: function(store, type, record) {
    var data = {
      id: record.get('id'),
      name: record.get('name'),
      phone: record.get('phone'),
      email: record.get('email')
    };
    return this.ajax(this.buildURL('update', null, record) + record.get('id'), "PUT", { data: data });
  },

  deleteRecord: function(store, type, record) {
    this.namespace = 'users/sign_out';
    return this.ajax(this.buildURL('delete', null, record), "DELETE", { data: '' });
  },

});
