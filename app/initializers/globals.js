import Ember from 'ember';
import ApplicationAdapter from '../adapters/application';

var globals = Ember.Object.extend({
  currentUser: null,
  userID: null,
  profile: null,

  getCurrentProfile: function () {
    var profile = JSON.parse(localStorage.getItem('profile'));    
    this.set('profile', profile);       
    return this.get('profile');
  },

  setCurrentProfile: function (profile) { 
    var profile = JSON.stringify(profile)    
    // this.set('profile', profile);    
    localStorage.setItem('profile', profile);
  }
});

export function initialize(container, application) {
  container.typeInjection('component', 'store', 'store:main');
  application.register('global:variables', globals, {singleton: true});
  application.inject('controller', 'globals', 'global:variables');
  application.inject('route', 'globals', 'global:variables');
  application.inject('model', 'globals', 'global:variables');
  application.inject('store', 'globals', 'global:variables');
  application.inject('adapter', 'globals', 'global:variables');
}

export default {
  name: 'globals',
  initialize: initialize
};