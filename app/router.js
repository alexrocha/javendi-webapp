import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route("home");
  this.route("signup");
  this.route("about");
  this.resource("ads", function() {
    this.route("new");
    this.route("show");
    
  });
  this.resource("main", function() {
    this.route("news");
    this.route("featured");
    this.route("sellers");
    this.route("specials");
    this.route("brands");
    this.route("reviews");
    this.route("contacts");
  });
});

export default Router;
